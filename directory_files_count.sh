#!/bin/bash
# Count files in the directory and store for prometheus node-exporter textfile collector
# Che 2024-02-13
#
export MONITORING_DIR=/app/monitoring/testfiles
export NE_TEXTFILE_DIR=/app/monitoring/node-exporter/textfile
export TARGET_FILE=$NE_TEXTFILE_DIR/files_count.prom
MONITORING_FILES_COUNT=$(ls $MONITORING_DIR/*.csv 2>/dev/null | wc -l) 
echo "
# HELP monitoring_files_count Number of files in monitoring directory
# TYPE monitoring_files_count gauge
monitoring_files_count ${MONITORING_FILES_COUNT}
" > $TARGET_FILE
